from pathlib import Path

import torch
from sklearn.metrics import f1_score, roc_auc_score
from torch.utils import data
import argparse

import datahandler
from model import createDeepLabv3
from trainer import train_model


def main():
    backbone_choices = ["resnet50", "resnet101", "mobilenet_v3_large"]

    parser = argparse.ArgumentParser(description="DeepLabv3 Training")
    parser.add_argument("--data-directory",
                        required=True,
                        help="Specify the data directory.")
    parser.add_argument("--exp-directory",
                        required=True,
                        help="Specify the experiment directory.")
    parser.add_argument("--epochs",
                        type=int,
                        required=True,
                        help="Specify the number of epochs you want to run the experiment for.")
    parser.add_argument("--batch-size",
                        default=4,
                        type=int,
                        help="Specify the batch size for the dataloader.")
    parser.add_argument("--backbone",
                        required=True,
                        type=str,
                        choices=backbone_choices,
                        help="Specify the model backbone (e.g., resnet50, resnet101, mobilenet_v3_large)")
    args = parser.parse_args()

    # Create the deeplabv3 model with the different backbones which is pretrained on a subset
    # of COCO train2017, on the 20 categories that are present in the Pascal VOC dataset.
    model = createDeepLabv3(args.backbone)
    model.train()
    data_directory = Path(args.data_directory)
    # Create the experiment directory if not present
    exp_directory = Path(args.exp_directory)
    if not exp_directory.exists():
        exp_directory.mkdir()

    # Specify the loss function
    criterion = torch.nn.MSELoss(reduction='mean')
    # Specify the optimizer with a lower learning rate
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

    # Specify the evaluation metrics
    metrics = {'f1_score': f1_score, 'auroc': roc_auc_score}

    # Create the dataloader for single folder
    # dataloaders = datahandler.get_dataloader_single_folder(
    #     data_directory, batch_size=batch_size)
    # _ = train_model(model,
    #                 criterion,
    #                 dataloaders,
    #                 optimizer,
    #                 bpath=exp_directory,
    #                 metrics=metrics,
    #                 num_epochs=epochs)

    # Create the dataloader for seperated folder
    dataloaders = datahandler.get_dataloader_sep_folder(
        data_directory, batch_size=args.batch_size)
    for epoch in range(1, args.epochs + 1):
        _ = train_model(model,
                        criterion,
                        dataloaders,
                        optimizer,
                        bpath=exp_directory,
                        metrics=metrics,
                        num_epochs=args.epochs,
                        backbone_name=args.backbone,
                        current_epoch=epoch)

    # Save the trained model
    torch.save(model, exp_directory / f'deeplabv3_{args.backbone}_weights.pt')

if __name__ == "__main__":
    main()











