""" DeepLabv3 Model download and change the head for your prediction"""
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
from torchvision import models


# def createDeepLabv3(outputchannels=1):
#     """DeepLabv3 class with custom head
#
#     Args:
#         outputchannels (int, optional): The number of output channels
#         in your dataset Masks. Defaults to 1.
#
#     Returns:
#         model: Returns the DeepLabv3 model with the different backbones.(resnet50,resnet101,mobilenet_v3_large)
#     """

def createDeepLabv3(backbone_name, outputchannels=1):
    # Choose the appropriate backbone model based on the model_name
    if backbone_name == "resnet50":
        model = models.segmentation.deeplabv3_resnet50(weights="DEFAULT", progress=True)
        model.classifier = DeepLabHead(2048, outputchannels)
    elif backbone_name == "resnet101":
        model = models.segmentation.deeplabv3_resnet101(weights="DEFAULT", progress=True)
        model.classifier = DeepLabHead(2048, outputchannels)
    elif backbone_name == "mobilenet_v3_large":
        model = models.segmentation.deeplabv3_mobilenet_v3_large(weights="DEFAULT", progress=True)
        model.classifier = DeepLabHead(960, outputchannels)
    else:
        raise ValueError("Invalid model_name. Available options are: resnet50, resnet101, mobilenet_v3_large")

    # Set the model in training mode
    model.train()
    return model
