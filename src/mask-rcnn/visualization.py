import matplotlib.pyplot as plt
from PIL import Image, ImageDraw

import numpy as np
import torch


import argparse
from models import get_model_instance_segmentation
from data_handler import Testset, get_transform


def visualize_results(model, dataset_test, img_idx):
    model.eval()

    img, _ = dataset_test[img_idx]  # Get a test image

    with torch.no_grad():
        prediction = model([img])

    img_ori = Image.fromarray(img.mul(255).permute(1, 2, 0).byte().numpy())
    draw = ImageDraw.Draw(img_ori)

    masks = prediction[0]['masks'].mul(255).byte().cpu().numpy().astype(np.uint8)
    masks_all = Image.fromarray(np.sum(np.sum(masks, axis=0), axis=0))

    for [x1, y1, x2, y2] in prediction[0]['boxes']:
        draw.rectangle([(x1, y1), (x2, y2)], outline=(255, 0, 0), width=2)

    imgs = [img_ori, masks_all]

    for i, im in enumerate(imgs):
        ax = plt.subplot(1, 2, i + 1)
        plt.tight_layout()
        ax.axis('off')
        plt.imshow(im)

    plt.show()


def main(args):
    # Load the model
    num_classes = 2  # Update with the correct number of classes
    model = get_model_instance_segmentation(num_classes,backbone=args.backbone)

    # Load the trained model parameters from the .pth file
    checkpoint_file = f'{args.backbone}_epoch={args.epochs}_best_state_dict.pth'
    model.load_state_dict(torch.load(checkpoint_file,map_location=torch.device('cpu')))

    # Load the evaluation results
    eval_results_file = f'{args.backbone}_epoch={args.epochs}_eval_results.txt'
    eval_results = []
    with open(eval_results_file, 'r') as f:
        for line in f:
            try:
                value = float(line.split(":")[1].strip())
                eval_results.append(value)
            except ValueError:
                print(f"Invalid format: {line.strip()}")

    # Print evaluating results
    for i, result in enumerate(eval_results):
        print(f"Epoch {i + 1}: {result}")

    # Draw the results
    plt.plot(range(1, len(eval_results) + 1), eval_results)
    plt.xlabel('Epoch')
    plt.ylabel('mAP')
    plt.title('Evaluation Results')
    plt.show()

    # Load the test dataset and transform
    test_data_dir = args.test_data_dir
    dataset_test = Testset(test_data_dir, get_transform(train=False))

    # Visualize the specified test image and its results
    visualize_results(model, dataset_test, args.img_idx)

    # Save the visualization result with the specified file name
    visualization_file = f'{args.backbone}_epoch={args.epochs}_img{args.img_idx}_result.png'
    plt.savefig(visualization_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Show segmentation results")
    parser.add_argument("--backbone",
                        type=str,
                        default="resnet50_fpn",
                        choices=["resnet50_fpn",
                                 "resnet18",
                                 "mobilenet_v2",
                                 "mobilenet_v3_small",
                                 "mobilenet_v3_large",
                                 "vgg16"],
                        help="Select the model architecture.")
    parser.add_argument("--epochs",
                        type=int,
                        default=10,
                        help="Number of epochs for training.")
    parser.add_argument("--img_idx",
                        type=int,
                        default=4,
                        help="Index of the test image you want to visualize.")
    parser.add_argument("--test_data_dir",
                        type=str,
                        default=None,
                        help="Path to the test data directory.")

    args = parser.parse_args()
    main(args)
