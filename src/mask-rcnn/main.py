import argparse
import os
import numpy as np
import torch

from numpy import random

import utils

from torch.utils.tensorboard import SummaryWriter

from engine import train_one_epoch, evaluate
from torch.utils.tensorboard import SummaryWriter
from data_handler import Trainset, Testset, get_transform
from models import get_model_instance_segmentation


def main():
    # Create a parser to handle command-line arguments
    parser = argparse.ArgumentParser(description="Mask-rcnnn Finetuning")
    parser.add_argument("--root",
                        type=str,
                        #default="/scratch/dcas/ha.yang/dataset",
                        help="Root directory where Train and Test folders are located.")

    parser.add_argument("--backbone",
                        type=str,
                        choices=["resnet50_fpn",
                                 "resnet18",
                                 "mobilenet_v2",
                                 "mobilenet_v3_small",
                                 "mobilenet_v3_large",
                                 "vgg16"],
                        help="Select the model architecture.")

    parser.add_argument("--epochs",
                        type=int,
                        #default=10,
                        help="Number of epochs for training.")
    args = parser.parse_args()

    # setting random seeds to fix the result of each training
    random_seed = 42
    random.seed(random_seed)
    np.random.seed(random_seed)
    torch.manual_seed(random_seed)
    torch.cuda.manual_seed_all(random_seed)
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # our dataset has two classes only - background and person
    num_classes = 2
    # use our dataset and defined transformations
    trainset_root = os.path.join(args.root, 'Train')
    testset_root = os.path.join(args.root, 'Test')
    dataset = Trainset(trainset_root, get_transform(train=True))
    dataset_test = Testset(testset_root, get_transform(train=False))

    # split the dataset in train and test set (it doesn't need here)
    # indices = torch.randperm(len(dataset)).tolist()
    # dataset = torch.utils.data.Subset(dataset, indices[:-50])
    # dataset_test = torch.utils.data.Subset(dataset_test, indices[-50:])

    # define training and validation data loaders
    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=2, shuffle=True, num_workers=4,
        collate_fn=utils.collate_fn)

    data_loader_test = torch.utils.data.DataLoader(
        dataset_test, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=utils.collate_fn)

    # get the model using our helper function
    model = get_model_instance_segmentation(num_classes, args.backbone)
    # move model to the right device
    model.to(device)
    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=0.005,
                                momentum=0.9, weight_decay=0.0005)
    # and a learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                   step_size=3,
                                                   gamma=0.1)

    # Total number of epochs for training
    num_epochs = args.epochs

    # Log directory for Tensorboard
    log_dir = f"logs_{args.backbone}_epoch={num_epochs}"
    # Comment for Tensorboard
    comment = f"epoch={num_epochs}"
    # Create a SummaryWriter object for Tensorboard
    writer = SummaryWriter(log_dir, comment)

    # Define the model checkpoint file name based on backbone and epochs
    checkpoint_file = f'./{args.backbone}_epoch={num_epochs}_best_state_dict.pth'
    best_mAp = 0
    eval_results = []
    for epoch in range(num_epochs):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq=8, writer=writer)
        # update the learning rate
        lr_scheduler.step()

        # evaluate on the test dataset
        eval_res = evaluate(model, data_loader_test, device=device, writer=writer)

        # save metrics
        mAp_epoch = float(eval_res.coco_eval['segm'].stats[0])
        eval_results.append(mAp_epoch)

        if mAp_epoch > best_mAp:
            torch.save(model.state_dict(), checkpoint_file)
            best_mAp = mAp_epoch
    print("That's it!")
    writer.close()

    eval_results_file = f'{args.backbone}_epoch={num_epochs}_eval_results.txt'
    with open(eval_results_file, 'w') as f:
        for i, result in enumerate(eval_results):
            f.write(f"Epoch {i + 1}: {result}\n")

        # start running here


if __name__ == "__main__":
    main()
