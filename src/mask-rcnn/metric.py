# import argparse
# import os
# import torch
# import cv2
# import numpy as np
# from PIL import Image
# from sklearn.metrics import f1_score, roc_auc_score
# from models import get_model_instance_segmentation
# from data_handler import Testset, get_transform
#
# def load_true_masks(mask_folder_path):
#     true_masks = []
#     max_shape = None  # 用于存储最大形状
#
#     for filename in os.listdir(mask_folder_path):
#         if filename.endswith(".png"):
#             mask_file_path = os.path.join(mask_folder_path, filename)
#             mask = cv2.imread(mask_file_path, cv2.IMREAD_GRAYSCALE)
#
#             if max_shape is None:
#                 max_shape = mask.shape  # 记录第一个掩码的形状
#
#             # 调整掩码的形状为最大形状
#             mask = cv2.resize(mask, (max_shape[1], max_shape[0]))
#
#             # Check if the range of 'unique_values' is [0, 255].
#             unique_values = np.unique(mask)
#             print("原始唯一值范围:", unique_values.min(), unique_values.max())
#
#             if unique_values.min() == 0 and unique_values.max() == 255:
#                 # Change the pixel value range from [0, 255] to [0, 1] with data type conversion
#                 mask = (mask > 0).astype(np.uint8)
#
#             # Verify the unique values after the change
#             unique_values_after_change = np.unique(mask)
#             print("更改后唯一值范围:", unique_values_after_change.min(), unique_values_after_change.max())
#
#             true_masks.append(mask)
#
#     # 检查所有掩码的形状是否一致
#     for mask in true_masks:
#         assert mask.shape == max_shape, "掩码的形状不一致"
#
#     # 检查所有掩码的唯一值范围
#     unique_values_in_true_masks = np.unique(np.concatenate(true_masks))
#     print("真实掩码中的唯一值范围:", unique_values_in_true_masks)
#
#     return true_masks
#
# def calculate_f1_score(true_mask, predicted_mask, threshold=0.5):
#     true_mask_binary = (true_mask > 0).astype(np.uint8)
#     predicted_mask_binary = (predicted_mask > threshold).astype(np.uint8)
#
#     f1 = f1_score(true_mask_binary.flatten(), predicted_mask_binary.flatten())
#
#     return f1
#
# def calculate_roc_auc(true_mask, predicted_mask, threshold=0.5):
#     true_mask_flat = true_mask.flatten()
#     predicted_mask_binary = (predicted_mask > threshold).astype(np.uint8)
#     predicted_mask_flat = predicted_mask_binary.flatten()
#
#     roc_auc = roc_auc_score(true_mask_flat, predicted_mask_flat)
#
#     return roc_auc
#
#
# def get_predicted_masks(model, img):
#     model.eval()
#
#     with torch.no_grad():
#         prediction = model([img])
#
#     masks = prediction[0]['masks']
#     masks_binary = (masks > 0.5).byte().cpu().numpy()
#
#     return masks_binary
#
#
# def main(args):
#     true_masks = load_true_masks(args.mask_folder_path)
#
#     num_classes = 2  # Update with the correct number of classes
#     model = get_model_instance_segmentation(num_classes, backbone=args.backbone)
#
#     checkpoint_file = f'{args.backbone}_epoch={args.epochs}_best_state_dict.pth'
#     model.load_state_dict(torch.load(checkpoint_file, map_location=torch.device('cpu')))
#
#     dataset_test = Testset(args.test_data_dir, get_transform(train=False))
#
#     f1_scores = []
#     roc_auc_scores = []
#
#
#     predicted_masks_list = []  # 创建一个空的列表来存储所有预测掩码
#
#     for img_idx in range(len(dataset_test)):  # Iterate over all images
#         img, _ = dataset_test[img_idx]
#         predicted_masks = get_predicted_masks(model, img)
#
#         # 如果没有有效的预测掩码，添加一个空的掩码并继续
#         if predicted_masks.size == 0:
#             empty_mask = np.zeros((1, 530, 500), dtype=np.uint8)  # 创建一个空的掩码
#             predicted_masks_list.append(empty_mask)
#             continue
#
#         # 调整每个预测掩码的形状为 (530, 500)
#         resized_masks = []
#         for mask in predicted_masks:
#             resized_mask = cv2.resize(mask[0], (500, 530), interpolation=cv2.INTER_NEAREST)
#             resized_masks.append(resized_mask)
#
#         # 合并多个调整后的预测掩码成一个
#         merged_mask = np.max(resized_masks, axis=0)
#
#         # 调整合并后的掩码形状为 (1, 530, 500)
#         merged_mask = merged_mask[np.newaxis, ...]
#
#         predicted_masks_list.append(merged_mask)  # 将每个图像的合并后的预测掩码添加到列表中
#
#         # 打印合并后的掩码的形状
#         print(f"合并后的掩码形状：{merged_mask.shape}")
#         # 打印合并后的掩码的值
#         unique_values = np.unique(merged_mask)
#         print(f"合并后的掩码的值：{unique_values}")
#
#     # 打印 predicted_masks_list 的数量
#     print(f"predicted_masks_list 的数量：{len(predicted_masks_list)}")
#     print(f"true_masks 的数量：{len(true_masks)}")
#
# # Calculate F1 and ROC AUC for each image's true and predicted masks
#
#
#
#     for i in range(len(true_masks)):
#         true_mask_np = np.array(true_masks[i])  # 将true_mask转换为NumPy数组
#         f1 = calculate_f1_score(true_mask_np, predicted_masks_list[i], threshold=args.threshold)
#         f1_scores.append(f1)
#
#         roc_auc = calculate_roc_auc(true_mask_np, predicted_masks_list[i], threshold=args.threshold)
#         roc_auc_scores.append(roc_auc)
#     avg_f1_score = np.mean(f1_scores)
#     avg_roc_auc_score = np.mean(roc_auc_scores)
#     print(f"Average F1 Score: {avg_f1_score:.4f}")
#     print(f"Average ROC AUC Score: {avg_roc_auc_score:.4f}")
#
#
#
# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(description="Mask RCNN Evaluation")
#     parser.add_argument("--mask_folder_path", type=str, default="path/to/mask/folder", help="Path to true mask folder")
#     parser.add_argument("--backbone", type=str, default="mobilenet_v2", help="Backbone model")
#     parser.add_argument("--epochs", type=int, default=10, help="Number of training epochs")
#     parser.add_argument("--test_data_dir", type=str, default="path/to/test/data", help="Path to test data directory")
#     parser.add_argument("--threshold", type=float, default=0.5, help="Threshold for binarizing masks")
#     args = parser.parse_args()
#
#     main(args)
#

import argparse
import os
import torch
import cv2
import numpy as np
from PIL import Image
from sklearn.metrics import f1_score, roc_auc_score
from models import get_model_instance_segmentation
from data_handler import Testset, get_transform

# Function to load true masks from a given folder path
def load_true_masks(mask_folder_path):
    true_masks = []
    max_shape = None  # Store the maximum shape encountered

    for filename in os.listdir(mask_folder_path):
        if filename.endswith(".png"):
            mask_file_path = os.path.join(mask_folder_path, filename)
            mask = cv2.imread(mask_file_path, cv2.IMREAD_GRAYSCALE)

            if max_shape is None:
                max_shape = mask.shape  # Record the shape of the first mask

            # Resize masks to the maximum shape encountered
            mask = cv2.resize(mask, (max_shape[1], max_shape[0]))

            # Check if the range of 'unique_values' is [0, 255]
            unique_values = np.unique(mask)
            # print("Original unique values range:", unique_values.min(), unique_values.max())

            if unique_values.min() == 0 and unique_values.max() == 255:
                # Convert pixel values from [0, 255] to [0, 1]
                mask = (mask > 0).astype(np.uint8)

            # Verify unique values after the change
            unique_values_after_change = np.unique(mask)
            # print("Unique values range after change:", unique_values_after_change.min(), unique_values_after_change.max())

            true_masks.append(mask)

    # Check if all masks have consistent shapes
    for mask in true_masks:
        assert mask.shape == max_shape, "Inconsistent mask shapes"

    # Check unique values across all true masks
    unique_values_in_true_masks = np.unique(np.concatenate(true_masks))
    # print("Unique values range in true masks:", unique_values_in_true_masks)

    return true_masks

# Function to calculate F1 score for a true mask and predicted mask
def calculate_f1_score(true_mask, predicted_mask, threshold=0.5):
    true_mask_binary = (true_mask > 0).astype(np.uint8)
    predicted_mask_binary = (predicted_mask > threshold).astype(np.uint8)

    f1 = f1_score(true_mask_binary.flatten(), predicted_mask_binary.flatten())

    return f1

# Function to calculate ROC AUC for a true mask and predicted mask
def calculate_roc_auc(true_mask, predicted_mask, threshold=0.5):
    true_mask_flat = true_mask.flatten()
    predicted_mask_binary = (predicted_mask > threshold).astype(np.uint8)
    predicted_mask_flat = predicted_mask_binary.flatten()

    roc_auc = roc_auc_score(true_mask_flat, predicted_mask_flat)

    return roc_auc

# Function to get predicted masks from the model
def get_predicted_masks(model, img):
    model.eval()

    with torch.no_grad():
        prediction = model([img])

    masks = prediction[0]['masks']
    masks_binary = (masks > 0.5).byte().cpu().numpy()

    return masks_binary

# Main function
def main(args):
    true_masks = load_true_masks(args.mask_folder_path)

    num_classes = 2
    model = get_model_instance_segmentation(num_classes, backbone=args.backbone)

    checkpoint_file = f'{args.backbone}_epoch={args.epochs}_best_state_dict.pth'
    model.load_state_dict(torch.load(checkpoint_file, map_location=torch.device('cpu')))

    dataset_test = Testset(args.test_data_dir, get_transform(train=False))

    f1_scores = []
    roc_auc_scores = []

    predicted_masks_list = []

    for img_idx in range(len(dataset_test)):
        img, _ = dataset_test[img_idx]
        predicted_masks = get_predicted_masks(model, img)

        # If no valid predicted masks, add an empty mask and continue
        if predicted_masks.size == 0:
            empty_mask = np.zeros((1, 530, 500), dtype=np.uint8)
            predicted_masks_list.append(empty_mask)
            continue

        resized_masks = []
        for mask in predicted_masks:
            resized_mask = cv2.resize(mask[0], (500, 530), interpolation=cv2.INTER_NEAREST)
            resized_masks.append(resized_mask)

        merged_mask = np.max(resized_masks, axis=0)
        merged_mask = merged_mask[np.newaxis, ...]

        predicted_masks_list.append(merged_mask)

        # print(f"Merged mask shape: {merged_mask.shape}")
        unique_values = np.unique(merged_mask)
        # print(f"Merged mask values: {unique_values}")

    # print(f"Number of predicted masks: {len(predicted_masks_list)}")
    # print(f"Number of true masks: {len(true_masks)}")

    for i in range(len(true_masks)):
        true_mask_np = np.array(true_masks[i])
        f1 = calculate_f1_score(true_mask_np, predicted_masks_list[i], threshold=args.threshold)
        f1_scores.append(f1)

        roc_auc = calculate_roc_auc(true_mask_np, predicted_masks_list[i], threshold=args.threshold)
        roc_auc_scores.append(roc_auc)

        # Calculate average F1 and ROC AUC scores
    avg_f1_score = np.mean(f1_scores)
    avg_roc_auc_score = np.mean(roc_auc_scores)

    # Print the average scores
    print(f"Average F1 Score: {avg_f1_score:.4f}")
    print(f"Average ROC AUC Score: {avg_roc_auc_score:.4f}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mask RCNN Evaluation")
    parser.add_argument("--mask_folder_path", type=str, default="path/to/mask/folder", help="Path to true mask folder")
    parser.add_argument("--backbone", type=str, default="mobilenet_v2", help="Backbone model")
    parser.add_argument("--epochs", type=int, default=10, help="Number of training epochs")
    parser.add_argument("--test_data_dir", type=str, default="path/to/test/data", help="Path to test data directory")
    parser.add_argument("--threshold", type=float, default=0.5, help="Threshold for binarizing masks")
    args = parser.parse_args()

    main(args)

# #python metric.py --mask_folder_path C:\Users\Ana\Desktop\mask-rcnn_local\dataset\Test\Masks --backbone mobilenet_v2 --epochs 10 --test_data_dir C:\Users\Ana\Desktop\mask-rcnn_local\dataset\Test --threshold 0.5