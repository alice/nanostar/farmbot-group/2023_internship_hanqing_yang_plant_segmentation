import torch
import torchvision
from torch import nn
from torchvision.models.detection import MaskRCNN
from torchvision.models.detection.anchor_utils import AnchorGenerator
from torchvision.models import MobileNet_V2_Weights, ResNet18_Weights, MobileNet_V3_Small_Weights, \
    MobileNet_V3_Large_Weights, VGG16_Weights
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor


def get_model_instance_segmentation(num_classes, backbone):

    if backbone == "resnet50_fpn":
        # load an instance segmentation model pre-trained on COCO
        model = torchvision.models.detection.maskrcnn_resnet50_fpn_v2(weights="DEFAULT")
        # get number of input features for the classifier

        in_features = model.roi_heads.box_predictor.cls_score.in_features
        # replace the pre-trained head with a new one

        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
        # now get the number of input features for the mask classifier
        in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
        hidden_layer = 256
        # and replace the mask predictor with a new one
        model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                           hidden_layer,
                                                           num_classes)

    elif backbone == "resnet18":
        # Load the pretrained ResNet18 backbone.
        conv1 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).conv1
        bn1 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).bn1
        resnet18_relu = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).relu
        resnet18_max_pool = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).maxpool
        layer1 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).layer1
        layer2 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).layer2
        layer3 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).layer3
        layer4 = torchvision.models.resnet18(weights=ResNet18_Weights.DEFAULT).layer4
        backbone = nn.Sequential(
            conv1, bn1, resnet18_relu, resnet18_max_pool,
            layer1, layer2, layer3, layer4
        )
        # We need the output channels of the last convolutional layers from
        # the features for the Faster RCNN model.
        # It is 512 for ResNet18.
        backbone.out_channels = 512
        # let's make the RPN generate 5 x 3 anchors per spatial
        # location, with 5 different sizes and 3 different aspect
        # ratios. We have a Tuple[Tuple[int]] because each feature
        # map could potentially have different sizes and
        # aspect ratios
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))

        # let's define what are the feature maps that we will
        # use to perform the region of interest cropping, as well as
        # the size of the crop after rescaling.
        # if your backbone returns a Tensor, featmap_names is expected to
        # be ['0']. More generally, the backbone should return an
        # OrderedDict[Tensor], and in featmap_names you can choose which
        # feature maps to use.

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                        output_size=7,
                                                        sampling_ratio=2)

        mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                             output_size=14,
                                                             sampling_ratio=2)
        # put the pieces together inside a MaskRCNN model
        model = MaskRCNN(backbone,
                         num_classes,

                         rpn_anchor_generator=anchor_generator,
                         box_roi_pool=roi_pooler,
                         mask_roi_pool=mask_roi_pooler)

    elif backbone == "mobilenet_v2":
        # load a pre-trained model for classification and return
        # only the features
        backbone = torchvision.models.mobilenet_v2(weights=MobileNet_V2_Weights.DEFAULT).features

        # MaskRCNN needs to know the number of
        # output channels in a backbone. For mobilenet_v2, it's 1280
        # so we need to add it here,
        backbone.out_channels = 1280

        # let's make the RPN generate 5 x 3 anchors per spatial
        # location, with 5 different sizes and 3 different aspect
        # ratios. We have a Tuple[Tuple[int]] because each feature
        # map could potentially have different sizes and
        # aspect ratios
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                        output_size=7,
                                                        sampling_ratio=2)

        mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                             output_size=14,
                                                             sampling_ratio=2)
        # put the pieces together inside a MaskRCNN model
        model = MaskRCNN(backbone,
                         num_classes,
                         rpn_anchor_generator=anchor_generator,
                         box_roi_pool=roi_pooler,
                         mask_roi_pool=mask_roi_pooler)



    elif backbone == "mobilenet_v3_small":
            # load a pre-trained model for classification and return
            # only the features

            backbone = torchvision.models.mobilenet_v3_small(weights=MobileNet_V3_Small_Weights.DEFAULT).features

            # MaskRCNN needs to know the number of
            # output channels in a backbone.
            backbone.out_channels = 576

            # let's make the RPN generate 5 x 3 anchors per spatial
            # location, with 5 different sizes and 3 different aspect
            # ratios. We have a Tuple[Tuple[int]] because each feature
            # map could potentially have different sizes and
            # aspect ratios
            anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                               aspect_ratios=((0.5, 1.0, 2.0),))

            # let's define what are the feature maps that we will
            # use to perform the region of interest cropping, as well as
            # the size of the crop after rescaling.
            # if your backbone returns a Tensor, featmap_names is expected to
            # be ['0']. More generally, the backbone should return an
            # OrderedDict[Tensor], and in featmap_names you can choose which
            # feature maps to use.

            roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                            output_size=7,
                                                            sampling_ratio=2)

            mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                                 output_size=14,
                                                                 sampling_ratio=2)
            # put the pieces together inside a MaskRCNN model
            model = MaskRCNN(backbone,
                             num_classes,
                             rpn_anchor_generator=anchor_generator,
                             box_roi_pool=roi_pooler,
                             mask_roi_pool=mask_roi_pooler)




    elif backbone == "mobilenet_v3_large":
            # load a pre-trained model for classification and return
            # only the features
            backbone = torchvision.models.mobilenet_v3_large(weights=MobileNet_V3_Large_Weights.DEFAULT).features

            # MaskRCNN needs to know the number of
            # output channels in a backbone.
            backbone.out_channels = 960

            # let's make the RPN generate 5 x 3 anchors per spatial
            # location, with 5 different sizes and 3 different aspect
            # ratios. We have a Tuple[Tuple[int]] because each feature
            # map could potentially have different sizes and
            # aspect ratios
            anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                               aspect_ratios=((0.5, 1.0, 2.0),))

            # let's define what are the feature maps that we will
            # use to perform the region of interest cropping, as well as
            # the size of the crop after rescaling.
            # if your backbone returns a Tensor, featmap_names is expected to
            # be ['0']. More generally, the backbone should return an
            # OrderedDict[Tensor], and in featmap_names you can choose which
            # feature maps to use.

            roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                            output_size=7,
                                                            sampling_ratio=2)

            mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                                 output_size=14,
                                                                 sampling_ratio=2)
            # put the pieces together inside a MaskRCNN model
            model = MaskRCNN(backbone,
                             num_classes,
                             rpn_anchor_generator=anchor_generator,
                             box_roi_pool=roi_pooler,
                             mask_roi_pool=mask_roi_pooler)

    elif backbone == "vgg16":
        # load a pre-trained model for classification and return
        # only the features
        backbone = torchvision.models.vgg16(weights=VGG16_Weights.DEFAULT).features

        # MaskRCNN needs to know the number of
        # output channels in a backbone.
        backbone.out_channels = 512

        # let's make the RPN generate 5 x 3 anchors per spatial
        # location, with 5 different sizes and 3 different aspect
        # ratios. We have a Tuple[Tuple[int]] because each feature
        # map could potentially have different sizes and
        # aspect ratios
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))

        # let's define what are the feature maps that we will
        # use to perform the region of interest cropping, as well as
        # the size of the crop after rescaling.
        # if your backbone returns a Tensor, featmap_names is expected to
        # be ['0']. More generally, the backbone should return an
        # OrderedDict[Tensor], and in featmap_names you can choose which
        # feature maps to use.

        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                        output_size=7,
                                                        sampling_ratio=2)

        mask_roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                             output_size=14,
                                                             sampling_ratio=2)
        # put the pieces together inside a MaskRCNN model
        model = MaskRCNN(backbone,
                         num_classes,
                         rpn_anchor_generator=anchor_generator,
                         box_roi_pool=roi_pooler,
                         mask_roi_pool=mask_roi_pooler)


    else:
        raise ValueError(f"Invalid model name: {backbone}")

    return model
