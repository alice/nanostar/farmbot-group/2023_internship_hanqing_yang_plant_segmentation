import math
import sys
import time

import numpy as np
import cv2
import torch
import torchvision.models.detection.mask_rcnn
from torchvision.utils import make_grid
from skimage.measure import label, regionprops

import utils
from coco_eval import CocoEvaluator
from coco_utils import get_coco_api_from_dataset
import torchvision

from torchvision.transforms import transforms


def train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq, scaler=None, writer=None):
    model.train()
    metric_logger = utils.MetricLogger(delimiter="  ")
    metric_logger.add_meter("lr", utils.SmoothedValue(window_size=1, fmt="{value:.6f}"))
    header = f"Epoch: [{epoch}]"

    lr_scheduler = None
    if epoch == 0:
        warmup_factor = 1.0 / 1000
        warmup_iters = min(1000, len(data_loader) - 1)

        lr_scheduler = torch.optim.lr_scheduler.LinearLR(
            optimizer, start_factor=warmup_factor, total_iters=warmup_iters
        )
    update_count=0
    for images, targets in metric_logger.log_every(data_loader, print_freq, header):
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]
        with torch.cuda.amp.autocast(enabled=scaler is not None):
            loss_dict = model(images, targets)
            losses = sum(loss for loss in loss_dict.values())

        # reduce losses over all GPUs for logging purposes
        # 将图像和目标传递给模型，计算损失值。model是模型对象，它根据输入计算预测并返回损失字典。
        loss_dict_reduced = utils.reduce_dict(loss_dict)
        # 将损失字典中的所有损失项相加，得到总损失。
        losses_reduced = sum(loss for loss in loss_dict_reduced.values())
        # 将减少后的总损失转换为Python标量值。
        loss_value = losses_reduced.item()

        if not math.isfinite(loss_value):
            print(f"Loss is {loss_value}, stopping training")
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        # 如果使用混合精度
        if scaler is not None:
            scaler.scale(losses).backward()
            scaler.step(optimizer)
            scaler.update()
        else:
            losses.backward()
            optimizer.step()

        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced, **loss_dict_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])
        update_count+=1
        global_step = epoch * len(data_loader) + update_count
        if writer is not None and metric_logger.meters['loss'].count > 0:
            writer.add_scalar("train_mask_loss", metric_logger.meters['loss_mask'].global_avg,global_step)
            writer.add_scalar("learning_rate", metric_logger.meters['lr'].global_avg, global_step)
            writer.add_scalar("train_loss", metric_logger.meters['loss'].global_avg, global_step)

    return metric_logger
    # print(metric_logger)


def _get_iou_types(model):
    model_without_ddp = model
    if isinstance(model, torch.nn.parallel.DistributedDataParallel):
        model_without_ddp = model.module
    iou_types = ["bbox"]
    if isinstance(model_without_ddp, torchvision.models.detection.MaskRCNN):
        iou_types.append("segm")
    if isinstance(model_without_ddp, torchvision.models.detection.KeypointRCNN):
        iou_types.append("keypoints")
    return iou_types


@torch.inference_mode()
def evaluate(model, data_loader, device, writer=None):

    n_threads = torch.get_num_threads()
    # FIXME remove this and make paste_masks_in_image run on the GPU
    torch.set_num_threads(1)
    cpu_device = torch.device("cpu")
    model.eval()
    metric_logger = utils.MetricLogger(delimiter="  ")
    header = "Test:"

    coco = get_coco_api_from_dataset(data_loader.dataset)
    iou_types = _get_iou_types(model)
    coco_evaluator = CocoEvaluator(coco, iou_types)

    global_step = 0
    for images, targets in metric_logger.log_every(data_loader, 100, header):
        images = list(img.to(device) for img in images)

        if torch.cuda.is_available():
            torch.cuda.synchronize()
        model_time = time.time()
        outputs = model(images)
        outputs = [{k: v.to(cpu_device) for k, v in t.items()} for t in outputs]
        model_time = time.time() - model_time
        res = {target["image_id"].item(): output for target, output in zip(targets, outputs)}

        #print(targets[0]["image_id"].item())
        #print(targets)

        evaluator_time = time.time()
        coco_evaluator.update(res)
        evaluator_time = time.time() - evaluator_time
        metric_logger.update(model_time=model_time, evaluator_time=evaluator_time)

        # add every mask into Tensorboard
        # get corresponding image_id
        image_id = targets[0]['image_id'].item()
        #get masks from every outputs
        masks_tensor = outputs[0]['masks']
        for idx, mask in enumerate(masks_tensor):
            writer.add_images(str(image_id)+f'Mask_'+str(idx), mask, global_step,dataformats='CHW')
        global_step +=1

        # # merge all masks
        # image_id = targets[0]['image_id'].item()
        # # Iterate over the outputs list and merge the masks
        # merged_masks = []
        # for output in outputs:
        #     if "masks" in output:
        #         masks = output["masks"]
        #         if masks.numel() > 0:
        #             merged_mask = torch.sum(masks, dim=0)
        #             merged_masks.append(merged_mask)
        #
        # # Convert the merged masks into a tensor and create a visualization image
        # if merged_masks:
        #     merged_masks_tensor = torch.stack(merged_masks)
        #     merged_masks_grid = make_grid(merged_masks_tensor.unsqueeze(1))
        # writer.add_images(str(image_id),merged_masks_grid , global_step, dataformats='NCHW')



    # gather the stats from all processes
    metric_logger.synchronize_between_processes()
    print("Averaged stats:", metric_logger)
    coco_evaluator.synchronize_between_processes()

    # accumulate predictions from all images
    coco_evaluator.accumulate()
    coco_evaluator.summarize()
    torch.set_num_threads(n_threads)



    return coco_evaluator
