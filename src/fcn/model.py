""" FCN Model download and change the head for your prediction"""
from torchvision.models.segmentation.fcn import FCNHead
from torchvision import models



def createFCN(backbone_name, outputchannels=1):
    """FCN class with custom head
    #     Args:
    #         outputchannels (int, optional): The number of output channels
    #         in your dataset Masks. Defaults to 1.
    #
    #     Returns:
    #         model: Returns the FCN model with the different backbones.(resnet50, resnet101)
    #     """
    # Choose the appropriate backbone model based on the model_name
    if backbone_name == "resnet50":
        model = models.segmentation.fcn_resnet50(weights="DEFAULT", progress=True)
        model.classifier = FCNHead(2048, outputchannels)
    elif backbone_name == "resnet101":
        model = models.segmentation.fcn_resnet101(weights="DEFAULT", progress=True)
        model.classifier = FCNHead(2048, outputchannels)
    else:
        raise ValueError("Invalid model_name. Available options are: resnet50, resnet101")

    # Set the model in training mode
    model.train()
    return model