# Plant Segmentation 2023


## Project Overview

This project aims to compare the performance of different PyTorch semantic segmentation models and pretrained weights in the task of leaf segmentation. We investigate various models and different backbone combinations to assess their performance in segmenting plant leaves. Below is an example leaf sample along with its segmentation result:

![Leaf Sample](example.png)

## Key Features

- Compares different PyTorch semantic segmentation models in the task of leaf segmentation.
- Utilizes various backbones for experimentation to determine the optimal model architecture.
- Provides scripts for dataset download and formatting for easy acquisition and preparation of data.

## Project Structure
```console
Plant_Segmentation
│   README.md             # Project documentation
│   merge_data.py         # Dataset merging script
│   read_h5.py            # Utility script for reading HDF5 files
│
└───src
│   │
│   └───deeplabv3
│   │   │   deeplabv3_mobilenet_v3_large.slurm
│   │   │   deeplabv3_resnet50.slurm
│   │   │   deeplabv3_resnet101.slurm
│   │
│   └───mask-rcnn
│   │   │   mask-rcnn_mobilenet_v2.slurm
│   │   │   mask-rcnn_mobilenet_v3_small.slurm
│   │   │   mask-rcnn_mobilenet_v3_large.slurm
│   │   │   mask-rcnn_vgg16.slurm
│   │   │   mask-rcnn_resnet18.slurm
│   │   │   mask-rcnn_resnet50.slurm
│   │
│   └───fcn
│       │   fcn_resnet50.slurm
│       │   fcn_resnet101.slurm
│
└───dataset
    │
    └───Train
    │   │
    │   └───Images
    │   │   │   A1_plant001.png
    │   │   │   ...
    │   │   │   A4_plant0857.png
    │   │
    │   └───Masks
    │       │   A1_plant001.png
    │       │   ...
    │       │   A4_plant0857.png
    │
    └───Test
        │
        └───Images
        │   │   A1_plant003.png
        │   │   ...
        │   │   A4_plant1117.png
        │
        └───Masks
            │   A1_plant003.png
            │   ...
            │   A4_plant1117.png
```

## Dataset Download and Formatting

We have used the "LEAF SEGMENTATION AND COUNTING CHALLENGES" dataset, which has been preprocessed to create training, validation, and test sets. The dataset download link is as follows:

- Trainset Link: [https://fz-juelich.sciebo.de/s/oGWpbXTbyb9dI52/download](https://fz-juelich.sciebo.de/s/oGWpbXTbyb9dI52/download)
- Testset Link: [https://fz-juelich.sciebo.de/s/pm2MvVNljmBxfsr/download](https://fz-juelich.sciebo.de/s/pm2MvVNljmBxfsr/download)

*WARNING! Files are large and download may take a while.*

The dataset structure is as follows:

- The training set (Train) contains original images (Images) and their corresponding leaf masks (Masks).
- The test set (Test) also includes original images and their respective leaf masks.

## Model Evaluation

We use F1-score and ROC-AUC metrics from the scikit-learn library to evaluate the accuracy of models in the leaf segmentation task. These metrics will help you understand the performance differences between different model and backbone combinations.

>$$F1 = 2 * \frac{precision * recall}{precision + recall}$$
- F1-score Link: [https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html)
-  ROC-AUC  Link: [https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_auc_score.html](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.roc_auc_score.html)

Below are the equivalent steps for setting up the environment and running the commands in Python and Linux environments:

****

## Setup

### With the Anaconda prompt

#### Clone the Git repository
open Anaconda Prompt
```console
git clone https://gitlab.isae-supaero.fr/alice/nanostar/farmbot-group/2023_internship_hanqing_yang_plant_segmentation.git"
```

#### Change to the project directory
```console
cd 2023_internship_hanqing_yang_plant_segmentation/src/mask-rcnn
```
#### Create and activate the conda environment
```console
conda create -n myenv python=3.8
conda activate myenv
```

#### Install PyTorch and dependencies
```console
conda install pytorch torchvision torchaudio cpuonly -c pytorch  # for CPU
conda install pytorch torchvision torchaudio cudatoolkit=11.1 -c pytorch   # for GPU
conda install pytorch #version>=1.13.1 
pip install matplotlib,
pip install opencv-python,
pip install scikit-image,
pip install pycocotools,
pip install tensorboard,
pip install -U scikit-learn,
pip install tqdm
```
#### Change to the directory where the .h5 files are located
```console
cd your/path/to/.h5 files
```

#### Download the dataset files
```console
wget https://fz-juelich.sciebo.de/s/oGWpbXTbyb9dI52/download -O CVPPP2017_training_images.h5"
wget https://fz-juelich.sciebo.de/s/pm2MvVNljmBxfsr/download -O CVPPP2017_testing_images.h5"
```

#### Unzip the training dataset (if applicable)
```console
unzip CVPPP2017_training_images.h5
Unzip CVPPP2017_testing_images.h5
```

#### Get the Dataset from .h5 files
```console
python read_h5.py /your/local/path/CVPPP2017_training_images.h5 /your/local/path/CVPPP2017_testing_images.h5 /path/to/train_images /path/to/train_masks /path/to/test_images /path/to/test_masks
```

#### Merge all the images and Masks for Train and Test separately
```console
python merge_images.py /path/to/train_images/ /path/to/train_masks/ /path/to/dataset/Train/Images /path/to/dataset/Train/Masks

python merge_images.py /path/to/test_images/ /path/to/test_masks/ /path/to/dataset/Test/Images /path/to/dataset/Test/Masks
```

### On Ubuntu

#### Clone the Git repository
Open a terminal.
```console
git clone https://gitlab.isae-supaero.fr/alice/nanostar/farmbot-group/2023_internship_hanqing_yang_plant_segmentation.git
```
#### Change to the project directory
```console
cd 2023_internship_hanqing_yang_plant_segmentation/src/mask-rcnn
```
#### Create and activate the conda environment
```console
conda create -n mrcnn
conda activate mrcnn
```

#### Install PyTorch and dependencies
```console
conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia
conda install h5py
pip install matplotlib
pip install opencv-python
pip install scikit-image
pip install pycocotools
pip install tensorboard
pip install -U scikit-learn
pip install tqdm
```

#### Change to the directory where the .h5 files are located
```console
cd /scratch/dcas/f.lastname
wget https://fz-juelich.sciebo.de/s/oGWpbXTbyb9dI52/download -O CVPPP2017_training_images.h5
wget https://fz-juelich.sciebo.de/s/pm2MvVNljmBxfsr/download -O CVPPP2017_testing_images.h5
```
#### Unzip the training dataset (if applicable)
```console
unzip CVPPP2017_training_images.h5
unzip CVPPP2017_testing_images.h5
```


#### Get the Dataset from .h5 files
The format is as follow: 

 `python read_h5.py  path/to/train_h5_file  path/to/test_h5_file path/to/save_train_images  path/to/save_train_masks path/to/save_test_images  path/to/save_test_masks`

 ```console
python read_h5.py  /home/dcas/f.lastname/2023_internship_hanqing_yang_plant_segmentation/CVPPP2017_training_images.h5  /home/dcas/f.lastname/2023_internship_hanqing_yang_plant_segmentation/CVPPP2017_testing_images.h5  /scratch/dcas/f.lastname/train_images /scratch/dcas/f.lastname/train_masks /scratch/dcas/f.lastname/test_images /scratch/dcas/f.lastname/test_masks
```

#### Merge all the images and Masks for Train and Test separately
 The format is as follow:

 `python merge_images.py path/to/old_images_dir  path/to/old_masks_dir  path/to/new_images_dir path/to/new_masks_dir`

```console
python merge_images.py  /scratch/dcas/f.lastname/train_images/ /scratch/dcas/f.lastname/train_masks/ /scratch/dcas/f.lastname/dataset/Train/Images /scratch/dcas/f.lastname/dataset/Train/Masks

python merge_images.py  /scratch/dcas/f.lastname/test_images/ /scratch/dcas/f.lastname/test_masks/  /scratch/dcas/f.lastname/dataset/Test/Images /scratch/dcas/f.lastname/dataset/Test/Masks
```



***



## Train the pretrained segmentation models
- Train mask-rcnn
```console
conda activate myenv
python main.py --backbone mobilenet_v2 --epochs 10 --root /path/to/dataset
python main.py --backbone mobilenet_v3_small --epochs 10 --root /path/to/dataset
python main.py --backbone mobilenet_v3_large --epochs 10 --root /path/to/dataset
python main.py --backbone resnet18 --epochs 10 --root /path/to/dataset
python main.py --backbone resnet50_fpn --epochs 10 --root /path/to/dataset
python main.py --backbone vgg16 --epochs 10 --root /path/to/dataset
```
- Train deeplabv3
```console
conda activate myenv
python main.py --backbone mobilenet_v3_large --epochs 10 --data-directory /path/to/dataset --exp-directory /path/to/output/CFExp  #(Default in CFExp)

python main.py --backbone resnet101 --epochs 10 --data-directory /path/to/dataset --exp-directory /path/to/output/CFExp  #(Default in CFExp)

python main.py --backbone resnet50 --epochs 10 --data-directory /path/to/dataset --exp-directory /path/to/output/CFExp  #(Default in CFExp)
```
- Train fcn
```console
conda activate myenv
python main.py --backbone resnet101 --epochs 10 --data-directory /path/to/dataset --exp-directory /path/to/output/CFExp  #(Default in CFExp)

python main.py --backbone resnet50 --epochs 10 --data-directory /path/to/dataset --exp-directory /path/to/output/CFExp  #(Default in CFExp)
```

## Evaluation and Visualization
- mask-rcnn 
After training, you will obtain .pth files that store the trained models and weights. You can load the metric.py file to evaluate F1-score and ROC-AUC metrics.
$$ F_1 = 2\frac{Precision \times Recall}{Precision + Recall} $$
```console
python metric.py --mask_folder_path path/to/Masks --backbone mobilenet_v2 --epochs 10 --test_data_dir path/to/Test --threshold 0.5
```
You can use visualization.py to visualize the predicted masks.
```console
python visualization.py --backbone mobilenet_v2 --epochs 10 --img_idx 4 --test_data_dir /path/to/dataset/Test/
```
- deeplabv3 & fcn
open jupyter notebook, run documentation :Analysis.ipynb








***

## On the ISAE-SUPAERO supercomputer (Pando)

### Remote connexion
1. Go to the [Icare](icare.isae.fr) visualization service.
2. Click on "Calculateurs scientifiques".
3. Click on "pando".
4. Click on "pando1" or "pando2".
5. Log in with your ISAE username and password.

### Setup Pytorch
Use the following command lines to set up the conda environment.
```console
export http_proxy=http://proxy.isae.fr:3128
export https_proxy=http://proxy.isae.fr:3128
git clone https://gitlab.isae-supaero.fr/alice/nanostar/farmbot-group/2023_internship_hanqing_yang_plant_segmentation.git
cd 2023_internship_hanqing_yang_plant_segmentation/src/mask-rcnn
conda create -n mrcnn
conda activate mrcnn
conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.6 -c pytorch -c nvidia
conda install h5py
pip install matplotlib
pip install opencv-python
pip install scikit-image
pip install pycocotools
pip install tensorboard
pip install -U scikit-learn
pip install tqdm
```

### Download and format Dataset
```console
cd /scratch/dcas/f.lastname
 wget https://fz-juelich.sciebo.de/s/oGWpbXTbyb9dI52/download -O CVPPP2017_training_images.h5 
 wget https://fz-juelich.sciebo.de/s/pm2MvVNljmBxfsr/download -O CVPPP2017_testing_images.h5
unzip CVPPP2017_LSC_training.zip
```

Then training data is in
`/scratch/dcas/f.lastname/CVPPP2017_LSC_training/A*`.

Use the following command lines to:
1. get the Dataset from .h5 files. The format is as follow: 

 `python read_h5.py  path/to/train_h5_file  path/to/test_h5_file path/to/save_train_images  path/to/save_train_masks path/to/save_test_images  path/to/save_test_masks`

 ```console
python read_h5.py  /home/dcas/f.lastname/2023_internship_hanqing_yang_plant_segmentation/CVPPP2017_training_images.h5  /home/dcas/f.lastname/2023_internship_hanqing_yang_plant_segmentation/CVPPP2017_testing_images.h5  /scratch/dcas/f.lastname/train_images /scratch/dcas/f.lastname/train_masks /scratch/dcas/f.lastname/test_images /scratch/dcas/f.lastname/test_masks
```

2. merge all the images and Masks for Train and Test seperately. The format is as follow:

 `python merge_images.py path/to/old_images_dir  path/to/old_masks_dir  path/to/new_images_dir path/to/new_masks_dir`

```console

python merge_images.py  /scratch/dcas/f.lastname/train_images/ /scratch/dcas/f.lastname/train_masks/ /scratch/dcas/f.lastname/dataset/Train/Images /scratch/dcas/f.lastname/dataset/Train/Masks

python merge_images.py  /scratch/dcas/f.lastname/test_images/ /scratch/dcas/f.lastname/test_masks/  /scratch/dcas/f.lastname/dataset/Test/Images /scratch/dcas/f.lastname/dataset/Test/Masks
```

### Train
- Train mask-rcnn
```console
sbatch mask-rcnn.slurm  # or other .slurm documents
```
- Train deeplabv3
```console
sbatch deeplabv3.slurm  # or other .slurm documents
```
- Train fcn
```console
sbatch fcn.slurm  # or other .slurm documents
```

### Visualization
- mask-rcnn 
```console
python visualization.py --backbone mobilenet_v2 --epochs 1 --img_idx 4 --test_data_dir /path/to/dataset/Test/
```
- deeplabv3 & fcn
open jupyter notebook, run documentation :Analysis.ipynb



Feel free to contribute and provide improvement suggestions. We hope this project will assist you in achieving better results in the task of plant leaf segmentation.


***

## Results
|    Model Name    |    Backbone Name   |  Metrics    |   Metrics    |    
|:------------------:|:--------------------:|:------------:|:--------------:|
|              |               |Test_f1_score |  Test_rocauc   |
|     Mask-RCNN   |    mobilenet_v2   |   0.7644    |    0.8964    |
|              |  mobilenet_v3_large |   0.7363    |    0.8628    |
|              |  mobilenet_v3_small |   0.8307    |    0.9038    |
|              |        vgg16    |   0.8976    |    0.9385    |
|              |      resnet18    |   0.8514    |    0.9116    |
|              |      resnet50_fpn |   0.9377    |    0.9574    |
|     DeepLabV3   |  mobilenet_v3_large |   0.2465  |   0.9834   |
|              |      resnet50    |   0.2209   |   0.9768  |
|              |      resnet101   |   0.2467  |   0.9834   |
|     FCN       |      resnet50    |        0.24       |        0.98       |
|              |      resnet101   |        0.25       |        0.99       |

***

## References
 @inproceedings{he2017mask,
  title={Mask R-CNN},
  author={He, Kaiming and Gkioxari, Georgia and Doll{\'a}r, Piotr and Girshick, Ross},
  booktitle={Proceedings of the IEEE International Conference on Computer Vision},
  pages={2961--2969},
  year={2017}
}

@article{chen2017rethinking,
  title={Rethinking atrous convolution for semantic image segmentation},
  author={Chen, Liang-Chieh and Papandreou, George and Schroff, Florian and Adam, Hartwig},
  journal={arXiv preprint arXiv:1706.05587},
  year={2017}
}

@inproceedings{long2015fully,
  title={Fully convolutional networks for semantic segmentation},
  author={Long, Jonathan and Shelhamer, Evan and Darrell, Trevor},
  booktitle={Proceedings of the IEEE conference on computer vision and pattern recognition},
  pages={3431--3440},
  year={2015}
}

@article{li2021benchmarking,
  title={Benchmarking detection transfer learning with vision transformers},
  author={Li, Yanghao and Xie, Saining and Chen, Xinlei and Dollar, Piotr and He, Kaiming and Girshick, Ross},
  journal={arXiv preprint arXiv:2111.11429},
  year={2021}
}


