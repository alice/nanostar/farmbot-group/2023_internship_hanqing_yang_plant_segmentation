import os
import shutil
import argparse

def merge_files(old_images_dir, old_masks_dir, new_images_dir, new_masks_dir):
    os.makedirs(new_images_dir, exist_ok=True)
    os.makedirs(new_masks_dir, exist_ok=True)

    image_folders = os.listdir(old_images_dir)

    for folder in image_folders:
        image_files = os.listdir(os.path.join(old_images_dir, folder))
        mask_files = os.listdir(os.path.join(old_masks_dir, folder))

        for image_file, mask_file in zip(image_files, mask_files):
            if image_file.lower().endswith(('.png', '.jpg', '.jpeg')):
                new_image_name = folder + "_" + image_file
                new_mask_name = folder + "_" + mask_file

                shutil.copy(os.path.join(old_images_dir, folder, image_file), os.path.join(new_images_dir, new_image_name))
                shutil.copy(os.path.join(old_masks_dir, folder, mask_file), os.path.join(new_masks_dir, new_mask_name))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge test and train image and mask folders.")
    parser.add_argument("old_images_dir", help="Path to the old images directory")
    parser.add_argument("old_masks_dir", help="Path to the old masks directory")
    parser.add_argument("new_images_dir", help="Path to the new images directory")
    parser.add_argument("new_masks_dir", help="Path to the new masks directory")

    args = parser.parse_args()

    merge_files(args.old_images_dir, args.old_masks_dir, args.new_images_dir, args.new_masks_dir)

#run code below in command
# python merge_images.py dataset/train_images/ dataset/train_masks/ dataset/Train/images dataset/Train/masks
