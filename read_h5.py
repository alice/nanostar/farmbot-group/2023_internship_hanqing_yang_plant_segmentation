import os
import h5py
import numpy as np
from PIL import Image
import argparse

def process_h5_file(h5_file, output_folder_images, output_folder_masks):
    os.makedirs(output_folder_images, exist_ok=True)
    os.makedirs(output_folder_masks, exist_ok=True)

    with h5py.File(h5_file, "r") as f:
        for key in list(f.keys()):
            group = f[key]
            folder_path_fg = os.path.join(output_folder_masks, key)
            folder_path_rgb = os.path.join(output_folder_images, key)
            os.makedirs(folder_path_fg, exist_ok=True)
            os.makedirs(folder_path_rgb, exist_ok=True)

            for data_key in group.keys():
                subgroup = group[data_key]
                if "fg" in subgroup and "rgb" in subgroup:
                    data_fg = np.array(subgroup["fg"])
                    data_rgb = np.array(subgroup["rgb"])

                    # save masks
                    file_path_fg = os.path.join(folder_path_fg, f"{data_key}.png")
                    image_fg = Image.fromarray(data_fg)
                    image_fg.save(file_path_fg)
                    print(f"Saved FG image: {file_path_fg}")

                    # save images
                    file_path_rgb = os.path.join(folder_path_rgb, f"{data_key}.png")
                    image_rgb = Image.fromarray(data_rgb)
                    image_rgb.save(file_path_rgb)
                    print(f"Saved RGB image: {file_path_rgb}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process H5 files and save images and masks.")
    parser.add_argument("train_h5_file", help="Path to the H5 file containing trainset data")
    parser.add_argument("test_h5_file", help="Path to the H5 file containing testset data")
    parser.add_argument("output_folder_train_images", help="Path to the output folder for trainset images")
    parser.add_argument("output_folder_train_masks", help="Path to the output folder for trainset masks")
    parser.add_argument("output_folder_test_images", help="Path to the output folder for testset images")
    parser.add_argument("output_folder_test_masks", help="Path to the output folder for testset masks")

    args = parser.parse_args()

    # Process trainset data
    process_h5_file(args.train_h5_file, args.output_folder_train_images, args.output_folder_train_masks)

    # Process testset data
    process_h5_file(args.test_h5_file, args.output_folder_test_images, args.output_folder_test_masks)

# run it in command
# python read_h5.py  dataset/CVPPP2017_training_images.h5  dataset/CVPPP2017_testing_images.h5  dataset/train_images dataset/train_masks dataset/test_images dataset/test_masks